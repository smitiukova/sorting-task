import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class Sorting {

    public static final String FILE_NAME = "/Users/sonya/Library/Containers/pro.writer.mac/Data/Documents/numbers.txt";

    public static void main(String[] args) {

        try {
            Integer[] input = Files.lines(Paths.get(FILE_NAME), StandardCharsets.UTF_8).flatMap((p) ->
                    Arrays.asList(p.split(",")).stream()).map(s -> Integer.valueOf(s)).toArray(Integer[]::new);
            Arrays.sort(input);
            Arrays.stream(input).forEach(System.out::println);
            Arrays.sort(input, Collections.reverseOrder());
            Arrays.stream(input).forEach(System.out::println);

        } catch (IOException e) {
            System.out.println("IOException: исключение ввода-вывода.");
        }
    }
}
