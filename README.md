**Job assignment test for sorting numbers.**

---

**Steps:**

1. Read the file with numbers comma separated (with no spaces between).
2. Sort them in natural order and print in the console.
3. Sort them in decreasing order and print in the console

I used Java 8 tools: lambdas, metod references, Stream API.
